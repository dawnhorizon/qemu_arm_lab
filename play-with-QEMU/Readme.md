# qemu_arm_lab

#### 介绍
本项目用于QEMU环境中搭建ARM开发板环境相关的操作脚本。

#### 软件架构
本项目用于CentOS7下使用QEMU搭建ARM开发板环境相关的操作脚本。
主要包括以下几个步骤：
0. 安装依赖包
1. 搭建QEMU环境
2. 选择交叉编译器 arm-linux-gnueabi-gcc
3. 编译 Linux Kernel v.5.10
4. 使用BusyBox创建根文件系统rootfs
5. 使用virtio-9p文件系统实现QEMU guest与宿主机host共享目录
6. 进一步完善QEMU启动环境
7. Case: FIO测试
8. Case: 安装部署UBIFS

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
