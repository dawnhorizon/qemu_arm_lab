#!/bin/sh

echo ">> Downloading QEMU 5.1.0, other version see https://download.qemu.org/"

wget https://download.qemu.org/qemu-5.1.0.tar.xz

if [ $? -ne 0 ]; then
    echo "[Error] failed to download QEMU. "
    exit
else
    echo "succeed"
fi

tar -xvf qemu-5.1.0.tar.xz
cd qemu-5.1.0/

echo ">> Installing QEMU 5.1.0..."
echo ">>>> Configure QEMU for ARM arch... "
./configure  --target-list=arm-softmmu,mipsel-softmmu \
--enable-debug --enable-sdl \
--enable-kvm --enable-virtfs

make ; make install


