#!/bin/bash

PROJDIR=./
ROOTFS=${PROJDIR}/rootfs
BUSYBOX=./tools/busybox-1.27.0
ARM_GNUEABI_DIR=./tools/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabi

rm -rf ./rootfs
rm -f rootfs.ext3
rm -rf ./mnt_tmp

# 建立rootfs目录和启动设备和程序
## 建立rootfs中基本目录，eg. /dev /lib /etc中里面需要准备好一些启动脚本，后面我们直接拷贝进来
mkdir -p rootfs/{dev,lib}

## 拷贝准备好的/etc目录，里面包括启动脚本和系统目录的挂在命令（fstab)
cp -r ${PROJDIR}/filesystem/arm/etc rootfs/

## 拷贝busybox提供的基本用户指令 （bin  linuxrc  sbin  usr）
cp ${BUSYBOX}/_install/* -r rootfs/

## 拷贝交叉编译器提供的一些库
cp -P ${ARM_GNUEABI_DIR}/lib/* rootfs/lib/

# 创建搭载rootfs的启动介质
dd if=/dev/zero of=rootfs.ext3 bs=1M count=32
mkfs.ext3 rootfs.ext3

mkdir mnt_tmp
mount -t ext3 rootfs.ext3 ./mnt_tmp/ -o loop
cp -r rootfs/* ./mnt_tmp
umount ./mnt_tmp
