
1. 下载busybox源码，配置编译

```
cd tools/

curl http://busybox.net/downloads/busybox-1.27.0.tar.bz2 | tar xjf -
cd busybox-1.27.0/

make defconfig
```

2. 配置Busybox静态编译选项。（如果不使用静态编译，程序运行期间需要进行动态加载，则需在根文件系统中提供其所需的共享库， 否则qemu无法执行`sbin/init`)

```
make menuconfig

Location:                                                           
-> Busybox Settings                                        
      [*] Build BusyBox as a static binary (no shared libs)   
```


3. 执行交叉编译并安装
```
make CROSS_COMPILE=arm-linux-gnueabi-
make install CROSS_COMPILE=arm-linux-gnueabi-
```
