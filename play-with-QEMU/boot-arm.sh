KERNEL_DIR=./linux-5.10.50

qemu-system-arm -M vexpress-a9 -m 512M \
-dtb ${KERNEL_DIR}/arch/arm/boot/dts/vexpress-v2p-ca9.dtb \
-kernel ${KERNEL_DIR}/arch/arm/boot/zImage \
-nographic -append "root=/dev/mmcblk0 rw console=ttyAMA0" \
-sd rootfs.ext3
