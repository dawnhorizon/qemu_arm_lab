#!/bin/sh

PATH=$PATH:/host_files/install/sbin
dmesg -c ; dmesg -n 8

ubiformat /dev/mtd0 -y
ubiattach /dev/ubi_ctrl -m 0
mdev -s
ubimkvol /dev/ubi0 -s 64MiB -N ubi_vol0
mdev -s 
mkdir /ubifs
mount -t ubifs /dev/ubi0_0 /ubifs

cd /host_files/fio
./fio fio-rand-write.fio > fio.log


umount /ubifs
rmdir /ubifs
ubirmvol -N ubi_vol0 /dev/ubi0
ubidetach -d 0 /dev/ubi_ctrl


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PATH=$PATH:/host_files/install/sbin
dmesg -c ; dmesg -n 8

modprobe nandcore
modprobe nand_ecc
modprobe nand
cd /host_files
./load_nandsim.sh 256 256 2048

mdev -s
ubiformat /dev/mtd2 -y
ubiattach /dev/ubi_ctrl -m 2
mdev -s
ubimkvol /dev/ubi0 -s 100MiB -N ubi_vol0
mdev -s 
mkdir /ubifs
mount -t ubifs /dev/ubi0_0 /ubifs


./fio fio-rand-write.fio > fio2.log
